import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowCaseRoutingModule } from './show-case-routing.module';
import { ShowCasePageComponent } from './pages/show-case-page/show-case-page.component';


@NgModule({
  declarations: [
    ShowCasePageComponent
  ],
  imports: [
    CommonModule,
    ShowCaseRoutingModule
  ]
})
export class ShowCaseModule { }
