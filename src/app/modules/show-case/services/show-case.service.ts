import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';

import { delay } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ShowCaseService {

  constructor() { }

  getDataApi(): Observable<any> {
    //TODO: Aqui podemos hacer http.get('api...')
    const data = [
      {
        name: "Pudgy Penguin #1",
        price: 0.2,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
      {
        name: "Pudgy Penguin #2",
        price: 0.1,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
      {
        name: "Pudgy Penguin #3",
        price: 0.3,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
      {
        name: "Pudgy Penguin #4",
        price: 0.4,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
      {
        name: "Pudgy Penguin #5",
        price: 0.5,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
      {
        name: "Pudgy Penguin #6",
        price: 0.6,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
      {
        name: "Pudgy Penguin #7",
        price: 0.7,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
      {
        name: "Pudgy Penguin #8",
        price: 0.8,
        image: "https://media-exp1.licdn.com/dms/image/C4E03AQGqhulkmjwwVw/profile-displayphoto-shrink_800_800/0/1635435224551?e=1666828800&v=beta&t=SRi3vxINx06jWuQ48CHbhrSFymw3M_iZZ46_yD3nfs0"
      },
    ]

    return of(data).pipe(
      delay(1500)
    )
  }
}
