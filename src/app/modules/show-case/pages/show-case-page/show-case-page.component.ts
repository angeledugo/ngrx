import { Component,Input, OnInit } from '@angular/core';
import { ItemModel } from 'src/app/core/models/item.interface';
import { ShowCaseService } from '../../services/show-case.service';

@Component({
  selector: 'app-show-case-page',
  templateUrl: './show-case-page.component.html',
  styleUrls: ['./show-case-page.component.scss']
})
export class ShowCasePageComponent implements OnInit {
  @Input() item: any;
  listItems: ItemModel[] = []

  constructor(
    private showCase: ShowCaseService
  ) { }

  ngOnInit(): void {
    this.loadData()
  }


  loadData(): void {

    this.showCase.getDataApi()
      .subscribe((res) => {
        this.listItems = res

      })
  }

}
